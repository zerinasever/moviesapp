package com.example.zerinasever.moviesapp.di.module

import android.content.Context
import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.sharedPrefs.SharedPrefs
import com.example.zerinasever.moviesapp.sharedPrefs.SharedPrefsImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

private const val SHARED_PREFERENCES = "shared_preferences"

@Module
class SharedPrefsModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPrefs = SharedPrefsImpl(App.get().getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE))
}