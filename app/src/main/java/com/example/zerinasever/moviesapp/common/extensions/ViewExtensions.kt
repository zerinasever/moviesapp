package com.example.zerinasever.moviesapp.common.extensions

import android.content.Context
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.ui.custom.SimpleTextWatcher

fun View.hideKeyboard() {
    val manager: InputMethodManager? = App.get().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    manager?.let {
        manager.hideSoftInputFromWindow(windowToken, 0)
    }
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.onClick(onClick: () -> Unit) = setOnClickListener { onClick() }

fun EditText.onActionSearchClicked(onActionSearchClicked: () -> Unit) = setOnEditorActionListener { _, actionId, _ ->
    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
        onActionSearchClicked()
    }
    false
}

fun EditText.onTextChange(onTextChange: (String) -> Unit) = addTextChangedListener(SimpleTextWatcher { onTextChange(it) })