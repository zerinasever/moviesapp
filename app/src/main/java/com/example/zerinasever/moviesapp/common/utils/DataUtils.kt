package com.example.zerinasever.moviesapp.common.utils

import com.example.zerinasever.moviesapp.data.model.Genre
import com.example.zerinasever.moviesapp.data.model.Image
import com.example.zerinasever.moviesapp.data.model.Movie
import com.example.zerinasever.moviesapp.data.realm.RealmMovieModel
import java.math.BigDecimal

fun roundRating(rating: Float, decimalPlaces: Int): String {
    var bigDecimal = BigDecimal(rating.toString())
    bigDecimal = bigDecimal.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP)
    return bigDecimal.toString()
}

fun getFavouriteMoviesFromDatabase(realmFavouriteMovies: List<RealmMovieModel>?): List<Movie>? {
    val favouriteMovies: MutableList<Movie> = mutableListOf()
    realmFavouriteMovies?.run {
        realmFavouriteMovies.mapTo(favouriteMovies) { Movie(id = it.id, poster = it.poster, title = it.title, voteAverage = it.voteAverage) }
    }
    return favouriteMovies
}

fun getGenres(genres: List<Genre>): String {
    var genresFormatted = ""
    for (genre in genres) {
        genresFormatted += genre.name + "\n"
    }
    return genresFormatted
}

fun getImages(images: List<Image>): MutableList<String> {
    val imagePaths: MutableList<String> = mutableListOf()
    images.mapTo(imagePaths) { it.filePath }
    return imagePaths
}