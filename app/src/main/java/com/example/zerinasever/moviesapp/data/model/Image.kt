package com.example.zerinasever.moviesapp.data.model

import com.google.gson.annotations.SerializedName

data class Image(@SerializedName("aspect_ratio")
                 val aspectRadio: Float = 0f,
                 @SerializedName("file_path")
                 val filePath: String = "",
                 val height: Int = 0,
                 @SerializedName("iso_639_1")
                 val iso: String = "",
                 @SerializedName("vote_average")
                 val voteAverage: Float = 0f,
                 @SerializedName("vote_count")
                 val voteCount: Int = 0,
                 val width: Int = 0)