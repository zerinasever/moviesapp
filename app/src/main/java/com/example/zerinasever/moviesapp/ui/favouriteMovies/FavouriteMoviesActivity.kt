package com.example.zerinasever.moviesapp.ui.favouriteMovies

import android.content.Context
import android.content.Intent
import android.graphics.Canvas
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.R
import com.example.zerinasever.moviesapp.common.extensions.deleteFavouriteDialog
import com.example.zerinasever.moviesapp.common.extensions.gone
import com.example.zerinasever.moviesapp.common.extensions.onClick
import com.example.zerinasever.moviesapp.common.extensions.visible
import com.example.zerinasever.moviesapp.common.utils.drawOnSwipeView
import com.example.zerinasever.moviesapp.data.model.Movie
import com.example.zerinasever.moviesapp.ui.base.BaseActivity
import com.example.zerinasever.moviesapp.ui.movieDetails.MovieDetailsActivity
import com.example.zerinasever.moviesapp.ui.movies.list.MoviesAdapter
import kotlinx.android.synthetic.main.activity_favourite_movies.*
import javax.inject.Inject

class FavouriteMoviesActivity : BaseActivity(), FavouriteMoviesContract.View {

    @Inject
    lateinit var presenter: FavouriteMoviesContract.Presenter

    lateinit var moviesAdapter: MoviesAdapter

    companion object {

        fun getLaunchIntent(from: Context) = Intent(from, FavouriteMoviesActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite_movies)
        App.get().getComponent().inject(this)
        presenter.setView(this)
        initUi()
    }

    private fun initUi() {
        setAdapter()
        setItemTouchHelper()
        iconBack.onClick { presenter.iconBackClicked() }
    }

    private fun setAdapter() {
        moviesAdapter = MoviesAdapter({ presenter.itemClicked(it) }, { })
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = moviesAdapter
    }

    private fun setItemTouchHelper() {
        val itemTouchHelper = ItemTouchHelper(getSimpleItemTouchCallback())
        itemTouchHelper.attachToRecyclerView(recyclerView)
    }

    private fun getSimpleItemTouchCallback(): ItemTouchHelper.SimpleCallback {
        return object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val movieId = moviesAdapter.getMovieIdAtPosition(position)
                deleteFavouriteDialog(message = getString(R.string.delete_favourite_message),
                        yesAction = { presenter.clickedDeleteFavourite(position, movieId) },
                        noAction = { presenter.cancelDeletionFavourite(position) })
            }

            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean = false

            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    drawOnSwipeView(actionState, viewHolder, c, dX)
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }
    }

    override fun onResume() {
        presenter.getFavouriteMovies()
        super.onResume()
    }

    override fun deleteFavourite(position: Int) {
        moviesAdapter.removeMovie(position)
        checkIsListEmpty()
    }

    private fun checkIsListEmpty() {
        if (moviesAdapter.itemCount == 0) {
            showNoFavouritesText()
        }
    }

    override fun cancelSwipe(position: Int) = moviesAdapter.notifyItemRangeChanged(position, moviesAdapter.itemCount)

    override fun showFavouriteMovies(favouriteMovies: List<Movie>) = moviesAdapter.setMovies(favouriteMovies)

    override fun openMovieDetailsActivity(movieId: String) = startActivity(MovieDetailsActivity.getLaunchIntent(this, movieId))

    override fun showNoFavouritesText() {
        recyclerView.gone()
        noResultsLayout.visible()
    }

    override fun goBack() = finish()
}