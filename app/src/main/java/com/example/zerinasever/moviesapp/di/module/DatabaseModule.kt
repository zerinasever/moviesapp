package com.example.zerinasever.moviesapp.di.module

import com.example.zerinasever.moviesapp.database.DatabaseManager
import com.example.zerinasever.moviesapp.database.DatabaseManagerImpl
import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideRealmConfiguration(): RealmConfiguration = RealmConfiguration.Builder().build()

    @Singleton
    @Provides
    fun provideRealm(realmConfiguration: RealmConfiguration): Realm = Realm.getInstance(realmConfiguration)

    @Provides
    fun provideDatabase(realm: Realm): DatabaseManager = DatabaseManagerImpl(realm)
}