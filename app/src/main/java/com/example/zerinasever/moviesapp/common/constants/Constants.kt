package com.example.zerinasever.moviesapp.common.constants


//MOVIE TYPES
const val UPCOMING_MOVIES = "upcoming_movies"
const val POPULAR_MOVIES = "popular_movies"
const val TOP_RATED_MOVIES = "top_rated_movies"

//IMAGE PATHS
const val IMAGE_PATH_500_SIZE = "https://image.tmdb.org/t/p/w500/"
const val IMAGE_PATH_ORIGINAL_SIZE = "https://image.tmdb.org/t/p/original/"

//OTHER
const val MOVIE_ID = "movie_id"