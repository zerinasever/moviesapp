package com.example.zerinasever.moviesapp.presentation

import com.example.zerinasever.moviesapp.data.model.Movie
import com.example.zerinasever.moviesapp.networking.interaction.MoviesInteractor
import com.example.zerinasever.moviesapp.ui.searchMovies.SearchMoviesContract
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.disposables.CompositeDisposable
import java.io.IOException

class SearchMoviesPresenter(private val moviesInteractor: MoviesInteractor) : SearchMoviesContract.Presenter {

    private lateinit var view: SearchMoviesContract.View
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private var searchInput: String = ""

    override fun setView(view: SearchMoviesContract.View) {
        this.view = view
    }

    override fun searchInputChanged(searchInput: String) {
        this.searchInput = searchInput
        if (searchInput.isBlank()) {
            view.hideClearIcon()
        } else {
            view.showClearIcon()
        }
    }

    override fun searchMoviesByInput() {
        if (searchInput.isNotBlank()) {
            val disposable = moviesInteractor.searchMovies(searchInput,
                    onSuccess = { handleMoviesResponse(it.results) },
                    onError = { handleError(it) })
            compositeDisposable.add(disposable)
        }
    }

    private fun handleMoviesResponse(movies: MutableList<Movie>) {
        if (movies.isNotEmpty()) {
            view.showList()
            view.hideNoResultsLayout()
            view.hideKeyboard()
            view.setMovies(movies)
        } else {
            handleNoResults()
        }
    }

    private fun handleError(error: Throwable) {
        if (error is HttpException) {
            view.showServerError()
        } else if (error is IOException) {
            view.showNoInternetError()
        }
    }

    private fun handleNoResults() {
        view.hideList()
        view.showNoResultsLayout()
    }

    override fun iconClearClicked() {
        searchInput = ""
        view.clearSearchInput()
    }

    override fun iconCancelClicked() = view.goBack()

    override fun itemClicked(movieId: String) = view.openMovieDetailsActivity(movieId)

    override fun unSubscribe() = compositeDisposable.clear()
}