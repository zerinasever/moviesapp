package com.example.zerinasever.moviesapp.ui.movies.list

import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.zerinasever.moviesapp.R
import com.example.zerinasever.moviesapp.common.constants.IMAGE_PATH_500_SIZE
import com.example.zerinasever.moviesapp.common.extensions.onClick
import com.example.zerinasever.moviesapp.common.utils.loadImage
import com.example.zerinasever.moviesapp.common.utils.roundRating
import com.example.zerinasever.moviesapp.data.model.Movie
import kotlinx.android.synthetic.main.list_item_movie.view.*
import java.util.*

class MoviesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    lateinit var movie: Movie

    fun setMovieData(movie: Movie) {
        this.movie = movie
        with(itemView) {
            movieTitle.text = movie.title
            loadImage(movieImage, IMAGE_PATH_500_SIZE + movie.poster)
            rating.text = String.format(Locale.getDefault(), itemView.context.resources.getString(R.string.rating_format), roundRating(movie.voteAverage, 1))
        }
    }

    fun setOnItemClickListener(listener: (String) -> Unit) {
        itemView.onClick { listener(movie.id) }
    }
}