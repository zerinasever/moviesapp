package com.example.zerinasever.moviesapp.presentation

import com.example.zerinasever.moviesapp.ui.movieImage.MovieImageContract

class MovieImagePresenter : MovieImageContract.Presenter {

    private lateinit var view: MovieImageContract.View

    override fun setView(view: MovieImageContract.View) {
        this.view = view
    }

    override fun setMovieImageUrl(movieImageUrl: String?) {
        movieImageUrl?.run { view.showImage(this) }
    }
}