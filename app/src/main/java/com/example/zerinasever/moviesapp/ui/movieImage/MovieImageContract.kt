package com.example.zerinasever.moviesapp.ui.movieImage

import com.example.zerinasever.moviesapp.presentation.base.BasePresenter

interface MovieImageContract {

    interface View {

        fun showImage(movieImageUrl: String)
    }

    interface Presenter : BasePresenter<View> {

        fun setMovieImageUrl(movieImageUrl: String?)
    }
}