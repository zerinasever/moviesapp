package com.example.zerinasever.moviesapp.di

import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.di.module.*
import com.example.zerinasever.moviesapp.ui.favouriteMovies.FavouriteMoviesActivity
import com.example.zerinasever.moviesapp.ui.main.MainActivity
import com.example.zerinasever.moviesapp.ui.movieDetails.MovieDetailsActivity
import com.example.zerinasever.moviesapp.ui.movieImage.MovieImageActivity
import com.example.zerinasever.moviesapp.ui.movieImages.MovieImagesActivity
import com.example.zerinasever.moviesapp.ui.movies.MoviesFragment
import com.example.zerinasever.moviesapp.ui.searchMovies.SearchMoviesActivity
import com.example.zerinasever.moviesapp.ui.splash.SplashActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (PresentationModule::class), (NetworkInteractionModule::class),
    (DatabaseModule::class), (SharedPrefsModule::class)])
interface AppComponent {

    fun inject(app: App)

    fun inject(splashActivity: SplashActivity)

    fun inject(mainActivity: MainActivity)

    fun inject(moviesFragment: MoviesFragment)

    fun inject(movieDetailsActivity: MovieDetailsActivity)

    fun inject(movieImagesActivity: MovieImagesActivity)

    fun inject(movieImageActivity: MovieImageActivity)

    fun inject(favouriteMoviesActivity: FavouriteMoviesActivity)

    fun inject(searchMoviesActivity: SearchMoviesActivity)
}