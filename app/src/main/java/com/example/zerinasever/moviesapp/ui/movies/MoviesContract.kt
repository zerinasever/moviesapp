package com.example.zerinasever.moviesapp.ui.movies

import com.example.zerinasever.moviesapp.data.model.Movie
import com.example.zerinasever.moviesapp.presentation.base.BasePresenter

interface MoviesContract {

    interface View {

        fun setMovies(results: List<Movie>)

        fun openMovieDetailsActivity(movieId: String)

        fun showServerError()

        fun showNoInternetError()

        fun addMovies(results: MutableList<Movie>)
    }

    interface Presenter : BasePresenter<View> {

        fun setMovieType(movieType: String?)

        fun getMovies()

        fun itemClicked(movieId: String)

        fun onLastItemReached()

        fun unSubscribe()
    }
}