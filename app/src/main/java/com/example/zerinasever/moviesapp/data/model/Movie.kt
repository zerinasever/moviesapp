package com.example.zerinasever.moviesapp.data.model

import com.google.gson.annotations.SerializedName

data class Movie(@SerializedName("poster_path")
                 var poster: String = "",
                 var adult: Boolean = false,
                 var overview: String = "",
                 @SerializedName("release_date")
                 var releaseDate: String = "",
                 @SerializedName("genre_ids")
                 var genreIds: MutableList<Int> = mutableListOf(),
                 var id: String = "",
                 @SerializedName("original_language")
                 var originalLanguage: String = "",
                 var title: String = "",
                 @SerializedName("backdrop_path")
                 var backdropPath: String = "",
                 var popularity: Float = 0f,
                 @SerializedName("vote_count")
                 var voteCount: Int = 0,
                 var video: Boolean = false,
                 @SerializedName("vote_average")
                 var voteAverage: Float = 0f)