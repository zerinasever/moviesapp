package com.example.zerinasever.moviesapp.ui.movieImages.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.example.zerinasever.moviesapp.R

class MovieImagesAdapter(private val onClick: (String, ImageView) -> Unit) : RecyclerView.Adapter<MovieImagesHolder>() {

    private val movieImages: MutableList<String> = mutableListOf()

    fun setMovieImages(movieImages: List<String>) {
        this.movieImages.clear()
        this.movieImages.addAll(movieImages)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = movieImages.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieImagesHolder = MovieImagesHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_movie_image, parent, false))

    override fun onBindViewHolder(holder: MovieImagesHolder, position: Int) {
        val movieImage: String = movieImages[position]
        holder.setMovieImage(movieImage)
        holder.setOnItemClickListener(onClick)
    }
}