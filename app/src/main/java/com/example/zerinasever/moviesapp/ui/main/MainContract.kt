package com.example.zerinasever.moviesapp.ui.main

import com.example.zerinasever.moviesapp.presentation.base.BasePresenter

interface MainContract {

    interface View {

        fun goToSearchMoviesActivity()

        fun goToFavouriteMoviesActivity()
    }

    interface Presenter : BasePresenter<View> {

        fun iconSearchClicked()

        fun iconFavouritesClicked()
    }
}