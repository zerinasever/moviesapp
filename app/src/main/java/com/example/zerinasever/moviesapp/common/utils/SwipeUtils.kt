package com.example.zerinasever.moviesapp.common.utils

import android.graphics.*
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.R
import com.example.zerinasever.moviesapp.common.extensions.color

fun drawOnSwipeView(actionState: Int, viewHolder: RecyclerView.ViewHolder, c: Canvas, dX: Float) {
    val paint = Paint()
    val resources = App.get().resources
    if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
        with(viewHolder.itemView) {
            val height = bottom.toFloat() - top.toFloat()
            val width = height / 3
            context.color(R.color.colorAccent)
            val background = RectF(right.toFloat() + dX, top.toFloat(), right.toFloat(), bottom.toFloat())
            c.drawRect(background, paint)
            val icon: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.icon_delete)
            val iconDest = RectF(right.toFloat() - 2 * width, top.toFloat() + width, right.toFloat() - width, bottom.toFloat() - width)
            c.drawBitmap(icon, null, iconDest, paint)
        }
    }
}