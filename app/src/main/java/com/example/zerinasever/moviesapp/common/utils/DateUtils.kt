package com.example.zerinasever.moviesapp.common.utils

import com.example.zerinasever.moviesapp.common.extensions.isValid
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

private const val YEAR_FORMAT = "yyyy"
private const val DATE_FORMAT_BACKEND = "yyyy-MM-dd"

fun getYearFormat(dateToConvert: String): String = if (dateToConvert.isValid()) {
    val fromFormat: DateFormat = SimpleDateFormat(DATE_FORMAT_BACKEND, Locale.getDefault())
    val toFormat: DateFormat = SimpleDateFormat(YEAR_FORMAT, Locale.getDefault())
    val date: Date = fromFormat.parse(dateToConvert)
    toFormat.format(date)
} else {
    ""
}