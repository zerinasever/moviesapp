package com.example.zerinasever.moviesapp.ui.favouriteMovies

import com.example.zerinasever.moviesapp.data.model.Movie
import com.example.zerinasever.moviesapp.presentation.base.BasePresenter

interface FavouriteMoviesContract {

    interface View {

        fun showFavouriteMovies(favouriteMovies: List<Movie>)

        fun goBack()

        fun openMovieDetailsActivity(movieId: String)

        fun deleteFavourite(position: Int)

        fun cancelSwipe(position: Int)

        fun showNoFavouritesText()
    }

    interface Presenter : BasePresenter<View> {

        fun getFavouriteMovies()

        fun itemClicked(movieId: String)

        fun iconBackClicked()

        fun clickedDeleteFavourite(position: Int, movieId: String)

        fun cancelDeletionFavourite(position: Int)
    }
}