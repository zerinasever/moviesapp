package com.example.zerinasever.moviesapp.ui.searchMovies

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.R
import com.example.zerinasever.moviesapp.common.extensions.*
import com.example.zerinasever.moviesapp.data.model.Movie
import com.example.zerinasever.moviesapp.ui.base.BaseActivity
import com.example.zerinasever.moviesapp.ui.movieDetails.MovieDetailsActivity
import com.example.zerinasever.moviesapp.ui.movies.list.MoviesAdapter
import kotlinx.android.synthetic.main.activity_search_movies.*
import javax.inject.Inject

class SearchMoviesActivity : BaseActivity(), SearchMoviesContract.View {

    @Inject
    lateinit var presenter: SearchMoviesContract.Presenter

    private lateinit var moviesAdapter: MoviesAdapter

    companion object {

        fun getLaunchIntent(from: Context) = Intent(from, SearchMoviesActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_movies)
        App.get().getComponent().inject(this)
        presenter.setView(this)
        initUi()
    }

    private fun initUi() {
        setAdapter()
        iconClear.onClick { presenter.iconClearClicked() }
        iconCancel.onClick { presenter.iconCancelClicked() }
        searchInput.onTextChange { presenter.searchInputChanged(it) }
        searchInput.onActionSearchClicked { presenter.searchMoviesByInput() }
    }

    private fun setAdapter() {
        moviesAdapter = MoviesAdapter(onClick = { presenter.itemClicked(it) })
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = moviesAdapter
    }

    override fun setMovies(results: List<Movie>) = moviesAdapter.setMovies(results)

    override fun openMovieDetailsActivity(movieId: String) = startActivity(MovieDetailsActivity.getLaunchIntent(this, movieId))

    override fun hideClearIcon() {
        iconClear.fadeOut()
    }

    override fun showClearIcon() {
        iconClear.fadeIn()
    }

    override fun showList() {
        recyclerView.fadeIn()
    }

    override fun hideNoResultsLayout() {
        noResultsLayout.fadeOut()
    }

    override fun hideList() {
        recyclerView.fadeOut()
    }

    override fun showNoResultsLayout() {
        noResultsLayout.fadeIn()
    }

    override fun clearSearchInput() = searchInput.text.clear()

    override fun hideKeyboard() = rootLayout.hideKeyboard()

    override fun goBack() = finish()

    override fun showServerError() = toast(getString(R.string.server_error))

    override fun showNoInternetError() = toast(getString(R.string.no_internet_error))

    override fun onPause() {
        presenter.unSubscribe()
        super.onPause()
    }
}