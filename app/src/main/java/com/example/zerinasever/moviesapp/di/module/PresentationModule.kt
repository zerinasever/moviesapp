package com.example.zerinasever.moviesapp.di.module

import com.example.zerinasever.moviesapp.database.DatabaseManager
import com.example.zerinasever.moviesapp.networking.interaction.MoviesInteractor
import com.example.zerinasever.moviesapp.presentation.*
import com.example.zerinasever.moviesapp.sharedPrefs.SharedPrefs
import com.example.zerinasever.moviesapp.ui.favouriteMovies.FavouriteMoviesContract
import com.example.zerinasever.moviesapp.ui.main.MainContract
import com.example.zerinasever.moviesapp.ui.movieDetails.MovieDetailsContract
import com.example.zerinasever.moviesapp.ui.movieImage.MovieImageContract
import com.example.zerinasever.moviesapp.ui.movieImages.MovieImagesContract
import com.example.zerinasever.moviesapp.ui.movies.MoviesContract
import com.example.zerinasever.moviesapp.ui.searchMovies.SearchMoviesContract
import com.example.zerinasever.moviesapp.ui.splash.SplashContract
import dagger.Module
import dagger.Provides

@Module
class PresentationModule {

    @Provides
    fun provideSplashPresenter(moviesInteractor: MoviesInteractor, sharedPrefs: SharedPrefs): SplashContract.Presenter = SplashPresenter(moviesInteractor, sharedPrefs)

    @Provides
    fun provideMainPresenter(): MainContract.Presenter = MainPresenter()

    @Provides
    fun provideMoviesPresenter(moviesInteractor: MoviesInteractor): MoviesContract.Presenter = MoviesPresenter(moviesInteractor)

    @Provides
    fun provideMovieDetailsPresenter(moviesInteractor: MoviesInteractor, databaseManager: DatabaseManager): MovieDetailsContract.Presenter = MovieDetailsPresenter(moviesInteractor, databaseManager)

    @Provides
    fun provideMovieImagesPresenter(moviesInteractor: MoviesInteractor): MovieImagesContract.Presenter = MovieImagesPresenter(moviesInteractor)

    @Provides
    fun provideMovieImagePresenter(): MovieImageContract.Presenter = MovieImagePresenter()

    @Provides
    fun provideFavouriteMoviesPresenter(databaseManager: DatabaseManager): FavouriteMoviesContract.Presenter = FavouriteMoviesPresenter(databaseManager)

    @Provides
    fun provideSearchMoviesPresenter(moviesInteractor: MoviesInteractor): SearchMoviesContract.Presenter = SearchMoviesPresenter(moviesInteractor)
}
