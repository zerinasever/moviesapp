package com.example.zerinasever.moviesapp.ui.movieImages

import com.example.zerinasever.moviesapp.presentation.base.BasePresenter

interface MovieImagesContract {

    interface View {

        fun showNoMovieIdError()

        fun setImages(images: MutableList<String>)

        fun showServerError()

        fun showNoInternetError()

        fun goBack()
    }

    interface Presenter : BasePresenter<View> {

        fun setMovieId(id: String?)

        fun getMovieImages()

        fun iconBackClicked()

        fun unSubscribe()
    }
}