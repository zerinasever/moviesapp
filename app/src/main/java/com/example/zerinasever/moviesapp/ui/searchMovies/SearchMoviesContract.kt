package com.example.zerinasever.moviesapp.ui.searchMovies

import com.example.zerinasever.moviesapp.data.model.Movie
import com.example.zerinasever.moviesapp.presentation.base.BasePresenter

interface SearchMoviesContract {

    interface View {

        fun setMovies(results: List<Movie>)

        fun openMovieDetailsActivity(movieId: String)

        fun hideClearIcon()

        fun showClearIcon()

        fun showList()

        fun hideNoResultsLayout()

        fun hideList()

        fun showNoResultsLayout()

        fun clearSearchInput()

        fun goBack()

        fun hideKeyboard()

        fun showServerError()

        fun showNoInternetError()
    }

    interface Presenter : BasePresenter<View> {

        fun searchMoviesByInput()

        fun itemClicked(movieId: String)

        fun searchInputChanged(searchInput: String)

        fun iconClearClicked()

        fun iconCancelClicked()

        fun unSubscribe()
    }
}