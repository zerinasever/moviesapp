package com.example.zerinasever.moviesapp.common.utils

import android.widget.ImageView
import com.bumptech.glide.Glide

fun loadImage(image: ImageView?, imagePath: String) = image?.run {  Glide.with(context).load(imagePath).into(this)}