package com.example.zerinasever.moviesapp.ui.base

import android.support.v7.app.AppCompatActivity

open class BaseActivity : AppCompatActivity()