package com.example.zerinasever.moviesapp.data.response

import com.google.gson.annotations.SerializedName

data class TokenResponse(val success: Boolean = false,
                         @SerializedName("expires_at")
                         val expiresAt: String = "",
                         @SerializedName("request_token")
                         val requestToken: String = "")