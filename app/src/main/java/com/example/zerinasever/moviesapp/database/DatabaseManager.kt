package com.example.zerinasever.moviesapp.database

import com.example.zerinasever.moviesapp.data.realm.RealmMovieModel

interface DatabaseManager {

    fun addFavouriteMovie(movie: RealmMovieModel?)

    fun getFavouriteMovies(): List<RealmMovieModel>

    fun checkIfFavouriteExist(id: String): Boolean

    fun removeFavouriteMovie(id: String): Boolean
}