package com.example.zerinasever.moviesapp.ui.movies

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.R
import com.example.zerinasever.moviesapp.common.extensions.toast
import com.example.zerinasever.moviesapp.data.model.Movie
import com.example.zerinasever.moviesapp.ui.movieDetails.MovieDetailsActivity
import com.example.zerinasever.moviesapp.ui.movies.list.MoviesAdapter
import kotlinx.android.synthetic.main.fragment_movies.*
import javax.inject.Inject

class MoviesFragment : Fragment(), MoviesContract.View {

    @Inject
    lateinit var presenter: MoviesContract.Presenter

    private lateinit var moviesAdapter: MoviesAdapter

    companion object {
        private const val MOVIE_TYPE = "movie_type"

        fun newInstance(moviesType: String): Fragment {
            val fragment = MoviesFragment()
            val bundle = Bundle()
            bundle.putString(MOVIE_TYPE, moviesType)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.get().getComponent().inject(this)
        presenter.setView(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepareUi()
        prepareData()
    }

    private fun prepareUi() {
        setAdapter()
        getExtras()
    }

    private fun getExtras() {
        val bundle = arguments
        val movieType = bundle?.getString(MOVIE_TYPE) ?: ""
        presenter.setMovieType(movieType)
    }

    private fun prepareData() = presenter.getMovies()

    private fun setAdapter() {
        moviesAdapter = MoviesAdapter(onClick = { presenter.itemClicked(it) },
                onLastItemReached = { presenter.onLastItemReached() }
        )
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = moviesAdapter
    }

    override fun setMovies(results: List<Movie>) = moviesAdapter.setMovies(results)

    override fun openMovieDetailsActivity(movieId: String) {
        activity?.run { startActivity(MovieDetailsActivity.getLaunchIntent(this, movieId)) }
    }

    override fun showServerError() {
        activity?.toast(getString(R.string.server_error))
    }

    override fun showNoInternetError() {
        activity?.toast(getString(R.string.no_internet_error))
    }

    override fun addMovies(results: MutableList<Movie>) = moviesAdapter.addMoreMovies(results)

    override fun onPause() {
        presenter.unSubscribe()
        super.onPause()
    }
}