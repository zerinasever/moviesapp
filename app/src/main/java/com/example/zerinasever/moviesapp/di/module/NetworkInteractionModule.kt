package com.example.zerinasever.moviesapp.di.module

import com.example.zerinasever.moviesapp.networking.api.MoviesApiService
import com.example.zerinasever.moviesapp.networking.interaction.MoviesInteractor
import com.example.zerinasever.moviesapp.networking.interaction.MoviesInteractorImpl
import com.example.zerinasever.moviesapp.schedulers.SchedulerManager
import dagger.Module
import dagger.Provides

@Module(includes = [(BackendApiModule::class), (SchedulerModule::class)])
class NetworkInteractionModule {

    @Provides
    fun provideMoviesInteractor(schedulerManager: SchedulerManager, moviesApiService: MoviesApiService): MoviesInteractor = MoviesInteractorImpl(schedulerManager, moviesApiService)
}