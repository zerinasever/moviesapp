package com.example.zerinasever.moviesapp.presentation

import com.example.zerinasever.moviesapp.ui.main.MainContract

class MainPresenter : MainContract.Presenter {

    private lateinit var view: MainContract.View

    override fun setView(view: MainContract.View) {
        this.view = view
    }

    override fun iconSearchClicked() = view.goToSearchMoviesActivity()

    override fun iconFavouritesClicked() = view.goToFavouriteMoviesActivity()

}