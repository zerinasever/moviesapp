package com.example.zerinasever.moviesapp.presentation

import com.example.zerinasever.moviesapp.common.utils.getImages
import com.example.zerinasever.moviesapp.data.model.Image
import com.example.zerinasever.moviesapp.networking.interaction.MoviesInteractor
import com.example.zerinasever.moviesapp.ui.movieImages.MovieImagesContract
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.disposables.CompositeDisposable
import java.io.IOException

class MovieImagesPresenter(private val moviesInteractor: MoviesInteractor) : MovieImagesContract.Presenter {

    private lateinit var view: MovieImagesContract.View
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private var movieId: String = ""

    override fun setView(view: MovieImagesContract.View) {
        this.view = view
    }

    override fun setMovieId(id: String?) {
        id?.run { movieId = this }
    }

    override fun getMovieImages() {
        val disposable = moviesInteractor.getMovieImages(movieId,
                onSuccess = { handleMovieImages(it.backdrops) },
                onError = { handleError(it) })
        compositeDisposable.add(disposable)
    }

    private fun handleMovieImages(backdrops: MutableList<Image>) = view.setImages(getImages(backdrops))

    private fun handleError(error: Throwable) {
        if (error is HttpException) {
            view.showServerError()
        } else if (error is IOException) {
            view.showNoInternetError()
        }
    }

    override fun iconBackClicked() = view.goBack()

    override fun unSubscribe() = compositeDisposable.clear()
}