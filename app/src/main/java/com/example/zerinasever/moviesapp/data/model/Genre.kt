package com.example.zerinasever.moviesapp.data.model

data class Genre(val id: Int = 0,
                 val name: String = "")