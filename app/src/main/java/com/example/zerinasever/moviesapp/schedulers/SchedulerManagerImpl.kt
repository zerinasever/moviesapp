package com.example.zerinasever.moviesapp.schedulers

import io.reactivex.Scheduler

class SchedulerManagerImpl(private val mainThread: Scheduler, private val systemIO: Scheduler) : SchedulerManager {

    override fun mainThread(): Scheduler = mainThread

    override fun io(): Scheduler = systemIO
}