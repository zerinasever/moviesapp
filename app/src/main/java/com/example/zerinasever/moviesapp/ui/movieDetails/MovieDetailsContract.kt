package com.example.zerinasever.moviesapp.ui.movieDetails

import com.example.zerinasever.moviesapp.presentation.base.BasePresenter

interface MovieDetailsContract {

    interface View {

        fun showTitle(title: String, date: String)

        fun showImage(posterPath: String)

        fun showHomepage(homepageText: String)

        fun showOverview(overviewText: String)

        fun showGenres(genresText: String)

        fun goToMovieImagesActivity(movieId: String?)

        fun showRating(voteAverage: Float)

        fun hideHomepage()

        fun showTagline(taglineText: String)

        fun hideTagline()

        fun goToWebsite(homepage: String)

        fun goBack()

        fun showNoInternetError()

        fun showServerError()

        fun showRemoveFavouriteIcon()

        fun showFavouriteIcon()
    }

    interface Presenter : BasePresenter<View> {

        fun setMovieId(id: String?)

        fun checkIsMovieFavourite()

        fun getMovieDetails()

        fun imagesButtonClicked()

        fun iconBackClicked()

        fun homepageClicked()

        fun iconFavouriteClicked()

        fun unSubscribe()
    }
}