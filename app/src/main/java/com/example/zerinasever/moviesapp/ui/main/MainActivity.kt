package com.example.zerinasever.moviesapp.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.R
import com.example.zerinasever.moviesapp.common.constants.POPULAR_MOVIES
import com.example.zerinasever.moviesapp.common.constants.TOP_RATED_MOVIES
import com.example.zerinasever.moviesapp.common.constants.UPCOMING_MOVIES
import com.example.zerinasever.moviesapp.common.extensions.onClick
import com.example.zerinasever.moviesapp.ui.base.BaseActivity
import com.example.zerinasever.moviesapp.ui.favouriteMovies.FavouriteMoviesActivity
import com.example.zerinasever.moviesapp.ui.main.pager.MainPagerAdapter
import com.example.zerinasever.moviesapp.ui.movies.MoviesFragment
import com.example.zerinasever.moviesapp.ui.searchMovies.SearchMoviesActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainContract.View {

    @Inject
    lateinit var presenter: MainContract.Presenter

    companion object {
        fun getLaunchIntent(from: Context) = Intent(from, MainActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        App.get().getComponent().inject(this)
        presenter.setView(this)
        initUi()
    }

    private fun initUi() {
        initPager()
        iconSearch.onClick { presenter.iconSearchClicked() }
        iconFavourites.onClick { presenter.iconFavouritesClicked() }
    }

    private fun initPager() {
        val adapter = MainPagerAdapter(supportFragmentManager)
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 3
        tabLayout.setupWithViewPager(viewPager)
        adapter.addItem(MoviesFragment.newInstance(UPCOMING_MOVIES))
        adapter.addItem(MoviesFragment.newInstance(POPULAR_MOVIES))
        adapter.addItem(MoviesFragment.newInstance(TOP_RATED_MOVIES))
    }

    override fun goToSearchMoviesActivity() = startActivity(SearchMoviesActivity.getLaunchIntent(this))

    override fun goToFavouriteMoviesActivity() = startActivity(FavouriteMoviesActivity.getLaunchIntent(this))

}
