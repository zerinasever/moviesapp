package com.example.zerinasever.moviesapp.presentation

import com.example.zerinasever.moviesapp.common.constants.POPULAR_MOVIES
import com.example.zerinasever.moviesapp.common.constants.TOP_RATED_MOVIES
import com.example.zerinasever.moviesapp.common.constants.UPCOMING_MOVIES
import com.example.zerinasever.moviesapp.data.model.Movie
import com.example.zerinasever.moviesapp.networking.interaction.MoviesInteractor
import com.example.zerinasever.moviesapp.ui.movies.MoviesContract
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.disposables.CompositeDisposable
import java.io.IOException

class MoviesPresenter(private val moviesInteractor: MoviesInteractor) : MoviesContract.Presenter {

    private lateinit var view: MoviesContract.View
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    var type: String = ""
    var page: Int = 1

    override fun setView(view: MoviesContract.View) {
        this.view = view
    }

    override fun setMovieType(movieType: String?) {
        movieType?.run { type = movieType }
    }

    override fun getMovies() {
        when (type) {
            UPCOMING_MOVIES -> {
                val disposable = moviesInteractor.getUpcomingMovies(page,
                        onSuccess = { handleMoviesResponse(it.results, page) },
                        onError = { handleError(it) })
                compositeDisposable.add(disposable)
            }
            POPULAR_MOVIES -> {
                val disposable = moviesInteractor.getPopularMovies(page,
                        onSuccess = { handleMoviesResponse(it.results, page) },
                        onError = { handleError(it) })
                compositeDisposable.add(disposable)
            }
            TOP_RATED_MOVIES -> {
                val disposable = moviesInteractor.getTopRatedMovies(page,
                        onSuccess = { handleMoviesResponse(it.results, page) },
                        onError = { handleError(it) })
                compositeDisposable.add(disposable)
            }
        }
    }

    private fun handleMoviesResponse(movies: MutableList<Movie>, page: Int) {
        if (page == 1) {
            view.setMovies(movies)
        } else {
            view.addMovies(movies)
        }
    }

    private fun handleError(error: Throwable) {
        if (error is HttpException) {
            view.showServerError()
        } else if (error is IOException) {
            view.showNoInternetError()
        }
    }

    override fun onLastItemReached() {
        page++
        getMovies()
    }

    override fun itemClicked(movieId: String) = view.openMovieDetailsActivity(movieId)

    override fun unSubscribe() = compositeDisposable.clear()
}