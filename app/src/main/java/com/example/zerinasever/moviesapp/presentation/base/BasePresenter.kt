package com.example.zerinasever.moviesapp.presentation.base

interface BasePresenter<in T> {

    fun setView(view: T)
}