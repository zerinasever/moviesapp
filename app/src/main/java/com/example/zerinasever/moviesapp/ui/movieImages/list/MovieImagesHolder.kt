package com.example.zerinasever.moviesapp.ui.movieImages.list

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import com.example.zerinasever.moviesapp.common.constants.IMAGE_PATH_ORIGINAL_SIZE
import com.example.zerinasever.moviesapp.common.extensions.fadeIn
import com.example.zerinasever.moviesapp.common.extensions.fadeOut
import com.example.zerinasever.moviesapp.common.extensions.onClick
import kotlinx.android.synthetic.main.list_item_movie_image.view.*

class MovieImagesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var movieImagePath: String = ""

    fun setMovieImage(movieImagePath: String) {
        this.movieImagePath = movieImagePath

        val target = object : SimpleTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {
                with(itemView) {
                    movieImage.fadeIn()
                    progressBar.fadeOut()
                    movieImage.setImageBitmap(resource)
                }
            }
        }

        Glide.with(itemView.context).load(IMAGE_PATH_ORIGINAL_SIZE + movieImagePath).asBitmap().into(target)
    }

    fun setOnItemClickListener(listener: (String, ImageView) -> Unit) {
        itemView.onClick { listener(movieImagePath, itemView.movieImage) }
    }
}