package com.example.zerinasever.moviesapp.database

import com.example.zerinasever.moviesapp.data.realm.RealmMovieModel
import io.realm.Realm
import io.realm.RealmQuery
import io.realm.RealmResults

private const val ID = "id"

class DatabaseManagerImpl(val realm: Realm) : DatabaseManager {

    override fun addFavouriteMovie(movie: RealmMovieModel?) {
        movie?.run {
            realm.beginTransaction()
            realm.copyToRealmOrUpdate(this)
            realm.commitTransaction()
        }
    }

    override fun getFavouriteMovies(): List<RealmMovieModel> = realm.copyFromRealm(realm.where(RealmMovieModel::class.java).findAll())

    override fun checkIfFavouriteExist(id: String): Boolean {
        val query: RealmQuery<RealmMovieModel> = realm.where(RealmMovieModel::class.java).equalTo(ID, id)
        return query.count() != 0L
    }

    override fun removeFavouriteMovie(id: String): Boolean {
        realm.beginTransaction()
        val rows: RealmResults<RealmMovieModel> = realm.where(RealmMovieModel::class.java).equalTo(ID, id).findAll()
        val isRemoved = rows.deleteAllFromRealm()
        realm.commitTransaction()
        return isRemoved
    }
}