package com.example.zerinasever.moviesapp.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: Context) {

    @Provides
    @Singleton
    fun provideApp(): Context = app
}