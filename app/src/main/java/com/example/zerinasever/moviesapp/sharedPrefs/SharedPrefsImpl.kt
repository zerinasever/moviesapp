package com.example.zerinasever.moviesapp.sharedPrefs

import android.content.SharedPreferences

private const val REQUEST_TOKEN = "request_token"
private const val GUEST_SESSION_ID = "guest_session_id"

class SharedPrefsImpl(private val prefs: SharedPreferences) : SharedPrefs {

    private fun savePrefs(key: String, value: String?) {
        val editor: SharedPreferences.Editor = prefs.edit()
        editor.putString(key, value)
        editor.apply()
    }

    override fun setRequestToken(requestToken: String?) = savePrefs(key = REQUEST_TOKEN, value = requestToken)

    override fun getRequestToken(): String? = prefs.getString(REQUEST_TOKEN, "")

    override fun setGuestSessionId(sessionId: String?) = savePrefs(key = GUEST_SESSION_ID, value = sessionId)

    override fun getGuestSessionId(): String? = prefs.getString(GUEST_SESSION_ID, "")

}