package com.example.zerinasever.moviesapp.data.response

import com.example.zerinasever.moviesapp.data.model.Genre
import com.example.zerinasever.moviesapp.data.model.Language
import com.example.zerinasever.moviesapp.data.model.ProductionCompany
import com.example.zerinasever.moviesapp.data.model.ProductionCountry
import com.example.zerinasever.moviesapp.data.realm.RealmMovieModel
import com.google.gson.annotations.SerializedName

data class MovieResponse(val adult: Boolean = false,
                         @SerializedName("backdrop_path")
                         val backdropPath: String? = "",
                         @SerializedName("belongs_to_collection")
                         val belongsToCollection: Any,
                         val budget: Int = 0,
                         val genres: MutableList<Genre> = mutableListOf(),
                         val homepage: String = "",
                         val id: String = "",
                         @SerializedName("imdb_id")
                         val imdbId: String = "",
                         @SerializedName("original_language")
                         val originalLanguage: String = "",
                         @SerializedName("original_title")
                         val originalTitle: String = "",
                         val overview: String = "",
                         val popularity: Float = 0f,
                         @SerializedName("poster_path")
                         val posterPath: String = "",
                         @SerializedName("production_companies")
                         val productionCompanies: MutableList<ProductionCompany> = mutableListOf(),
                         @SerializedName("production_countries")
                         val productionCountries: MutableList<ProductionCountry> = mutableListOf(),
                         @SerializedName("release_date")
                         val releaseDate: String = "",
                         val revenue: Long = 0,
                         val runtime: Int = 0,
                         @SerializedName("spoken_languages")
                         val spokenLanguages: MutableList<Language> = mutableListOf(),
                         val status: String = "",
                         val tagline: String = "",
                         val title: String = "",
                         val video: Boolean = false,
                         @SerializedName("vote_average")
                         val voteAverage: Float = 0f,
                         @SerializedName("vote_count")
                         val voteCount: Int = 0)

fun MovieResponse.mapToRealmMovieModel(): RealmMovieModel {
    return RealmMovieModel(id = id,
            poster = posterPath,
            title = title,
            voteAverage = voteAverage)
}