package com.example.zerinasever.moviesapp.ui.movieImage

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.transition.Fade
import com.bumptech.glide.Glide
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.R
import com.example.zerinasever.moviesapp.common.constants.IMAGE_PATH_ORIGINAL_SIZE
import com.example.zerinasever.moviesapp.common.utils.loadImage
import com.example.zerinasever.moviesapp.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_movie_image.*
import javax.inject.Inject

class MovieImageActivity : BaseActivity(), MovieImageContract.View {

    @Inject
    lateinit var presenter: MovieImageContract.Presenter

    companion object {
        const val MOVIE_IMAGE_URL = "movie_image_url"

        fun getLaunchIntent(from: Context, movieImageUrl: String?): Intent {
            val intent = Intent(from, MovieImageActivity::class.java)
            intent.putExtra(MOVIE_IMAGE_URL, movieImageUrl)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.get().getComponent().inject(this)
        setContentView(R.layout.activity_movie_image)
        presenter.setView(this)
        initUi()
    }

    private fun initUi() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            postponeEnterTransition()
            val fade = Fade()
            fade.excludeTarget(android.R.id.statusBarBackground, true)
            fade.excludeTarget(android.R.id.navigationBarBackground, true)
            window.enterTransition = fade
            window.exitTransition = fade
        }
        getExtras()
    }

    private fun getExtras() {
        val bundle: Bundle? = intent.extras
        bundle?.run { presenter.setMovieImageUrl(bundle.getString(MOVIE_IMAGE_URL)) }
    }

    override fun showImage(movieImageUrl: String) {
        val target = object : SimpleTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startPostponedEnterTransition()
                    movieImage.setImageBitmap(resource)
                }
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Glide.with(this).load(IMAGE_PATH_ORIGINAL_SIZE + movieImageUrl).asBitmap().into(target)
        } else {
            loadImage(movieImage, movieImageUrl)
        }
    }
}