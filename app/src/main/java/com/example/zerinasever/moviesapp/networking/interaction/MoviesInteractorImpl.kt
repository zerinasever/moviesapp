package com.example.zerinasever.moviesapp.networking.interaction

import com.example.zerinasever.moviesapp.data.response.*
import com.example.zerinasever.moviesapp.networking.api.MoviesApiService
import com.example.zerinasever.moviesapp.schedulers.SchedulerManager
import io.reactivex.disposables.Disposable

private const val API_KEY = "9ee47b2f45979c257fb9aee92753500b"

class MoviesInteractorImpl(private val schedulerManager: SchedulerManager, private val moviesApiService: MoviesApiService) : MoviesInteractor {

    override fun getUserToken(onSuccess: (TokenResponse) -> Unit, onError: (Throwable) -> Unit): Disposable =
            moviesApiService.getUserToken(API_KEY).subscribeOn(schedulerManager.io()).observeOn(schedulerManager.mainThread()).subscribe(
                    { onSuccess(it) },
                    { onError(it) }
            )

    override fun authenticateUser(requestToken: String, onSuccess: (TokenResponse) -> Unit, onError: (Throwable) -> Unit): Disposable =
            moviesApiService.authenticateUser(API_KEY, requestToken).subscribeOn(schedulerManager.io()).observeOn(schedulerManager.mainThread()).subscribe(
                    { onSuccess(it) },
                    { onError(it) })

    override fun createGuestSession(onSuccess: (GuestSessionResponse) -> Unit, onError: (Throwable) -> Unit): Disposable =
            moviesApiService.createGuestSession(API_KEY).subscribeOn(schedulerManager.io()).observeOn(schedulerManager.mainThread()).subscribe(
                    { onSuccess(it) },
                    { onError(it) })

    override fun createSession(requestToken: String, onSuccess: (SessionResponse) -> Unit, onError: (Throwable) -> Unit): Disposable =
            moviesApiService.createSession(API_KEY, requestToken).subscribeOn(schedulerManager.io()).observeOn(schedulerManager.mainThread()).subscribe(
                    { onSuccess(it) },
                    { onError(it) })

    override fun searchMovies(query: String, onSuccess: (MoviesResponse) -> Unit, onError: (Throwable) -> Unit): Disposable =
            moviesApiService.searchMovies(API_KEY, query).subscribeOn(schedulerManager.io()).observeOn(schedulerManager.mainThread()).subscribe(
                    { onSuccess(it) },
                    { onError(it) })

    override fun getMovieDetails(movieId: String, onSuccess: (MovieResponse) -> Unit, onError: (Throwable) -> Unit): Disposable =
            moviesApiService.getMovieDetails(movieId, API_KEY).subscribeOn(schedulerManager.io()).observeOn(schedulerManager.mainThread()).subscribe(
                    { onSuccess(it) },
                    { onError(it) })

    override fun getPopularMovies(page: Int, onSuccess: (MoviesResponse) -> Unit, onError: (Throwable) -> Unit): Disposable =
            moviesApiService.getPopularMovies(API_KEY, page).subscribeOn(schedulerManager.io()).observeOn(schedulerManager.mainThread()).subscribe(
                    { onSuccess(it) },
                    { onError(it) })

    override fun getUpcomingMovies(page: Int, onSuccess: (MoviesResponse) -> Unit, onError: (Throwable) -> Unit): Disposable =
            moviesApiService.getUpcomingMovies(API_KEY, page).subscribeOn(schedulerManager.io()).observeOn(schedulerManager.mainThread()).subscribe(
                    { onSuccess(it) },
                    { onError(it) })

    override fun getTopRatedMovies(page: Int, onSuccess: (MoviesResponse) -> Unit, onError: (Throwable) -> Unit): Disposable =
            moviesApiService.getTopRatedMovies(API_KEY, page).subscribeOn(schedulerManager.io()).observeOn(schedulerManager.mainThread()).subscribe(
                    { onSuccess(it) },
                    { onError(it) })

    override fun getMovieImages(movieId: String, onSuccess: (MovieImagesResponse) -> Unit, onError: (Throwable) -> Unit): Disposable =
            moviesApiService.getMovieImages(movieId, API_KEY).subscribeOn(schedulerManager.io()).observeOn(schedulerManager.mainThread()).subscribe(
                    { onSuccess(it) },
                    { onError(it) })
}