package com.example.zerinasever.moviesapp.presentation

import com.example.zerinasever.moviesapp.networking.interaction.MoviesInteractor
import com.example.zerinasever.moviesapp.sharedPrefs.SharedPrefs
import com.example.zerinasever.moviesapp.ui.splash.SplashContract
import io.reactivex.disposables.CompositeDisposable

class SplashPresenter(private val moviesInteractor: MoviesInteractor,
                      private val sharedPrefs: SharedPrefs) : SplashContract.Presenter {

    private lateinit var view: SplashContract.View
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun setView(view: SplashContract.View) {
        this.view = view
    }

    override fun createGuestSession() {
        sharedPrefs.getGuestSessionId()?.run {
            if (this.isNotBlank()) {
                view.goToMain()
            } else {
                val disposable = moviesInteractor.createGuestSession(
                        onSuccess = { handleSuccess(it.guestSessionId) },
                        onError = { view.goToMain() })
                compositeDisposable.add(disposable)
            }
        }
    }

    private fun handleSuccess(guestSessionId: String) {
        sharedPrefs.setGuestSessionId(guestSessionId)
        view.goToMain()
    }

    override fun unSubscribe() = compositeDisposable.clear()
}