package com.example.zerinasever.moviesapp.common.extensions

import android.text.SpannableString
import android.text.style.UnderlineSpan

fun String?.isValid() = this != null && isNotEmpty() && trim().isNotEmpty()

fun String.underlineText(): SpannableString {
    val content = SpannableString(this)
    content.setSpan(UnderlineSpan(), 0, content.length, 0)
    return content
}