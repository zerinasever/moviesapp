package com.example.zerinasever.moviesapp.data.response

import com.example.zerinasever.moviesapp.data.model.Movie

data class MoviesResponse(val page: Int = 0,
                          val results: MutableList<Movie> = mutableListOf())