package com.example.zerinasever.moviesapp

import android.app.Application
import com.example.zerinasever.moviesapp.di.AppComponent
import com.example.zerinasever.moviesapp.di.DaggerAppComponent
import io.realm.Realm

class App : Application() {

    companion object {
        private lateinit var sInstance: App
        lateinit var component: AppComponent

        fun get(): App = sInstance

        fun setInstance(instance: App) {
            sInstance = instance
        }
    }

    override fun onCreate() {
        super.onCreate()
        setInstance(this)

        component = DaggerAppComponent.builder().build()
        component.inject(this)

        Realm.init(this)
    }

    fun getComponent(): AppComponent = component
}