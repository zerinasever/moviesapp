package com.example.zerinasever.moviesapp.data.response

import com.google.gson.annotations.SerializedName

data class GuestSessionResponse(val success: Boolean = false,
                                @SerializedName("guest_session_id")
                                val guestSessionId: String = "",
                                @SerializedName("expires_at")
                                val expiresAt: String = "")