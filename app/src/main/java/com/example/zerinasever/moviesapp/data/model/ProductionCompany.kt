package com.example.zerinasever.moviesapp.data.model

data class ProductionCompany(val name: String = "",
                             val id: Int = 0)