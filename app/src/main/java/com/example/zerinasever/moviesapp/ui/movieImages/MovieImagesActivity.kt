package com.example.zerinasever.moviesapp.ui.movieImages

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.widget.ImageView
import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.R
import com.example.zerinasever.moviesapp.common.constants.MOVIE_ID
import com.example.zerinasever.moviesapp.common.extensions.onClick
import com.example.zerinasever.moviesapp.common.extensions.toast
import com.example.zerinasever.moviesapp.ui.base.BaseActivity
import com.example.zerinasever.moviesapp.ui.movieImage.MovieImageActivity
import com.example.zerinasever.moviesapp.ui.movieImages.list.MovieImagesAdapter
import kotlinx.android.synthetic.main.activity_movie_images.*
import javax.inject.Inject

class MovieImagesActivity : BaseActivity(), MovieImagesContract.View {

    @Inject
    lateinit var presenter: MovieImagesContract.Presenter

    private lateinit var movieImagesAdapter: MovieImagesAdapter

    companion object {

        fun getLaunchIntent(from: Context, movieId: String?): Intent {
            val intent = Intent(from, MovieImagesActivity::class.java)
            intent.putExtra(MOVIE_ID, movieId)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.get().getComponent().inject(this)
        setContentView(R.layout.activity_movie_images)
        presenter.setView(this)
        initUi()
    }

    private fun initUi() {
        getExtras()
        setListeners()
        setAdapter()
    }

    private fun getExtras() {
        val bundle: Bundle? = intent.extras
        bundle?.run {
            presenter.setMovieId(bundle.getString(MOVIE_ID))
            presenter.getMovieImages()
        }
    }

    private fun setListeners() {
        iconBack.onClick { presenter.iconBackClicked() }
    }

    private fun setAdapter() {
        movieImagesAdapter = MovieImagesAdapter(
                onClick = { url, image -> goToMovieImageActivity(url, image) }
        )
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.adapter = movieImagesAdapter
    }

    override fun showNoMovieIdError() = toast(getString(R.string.no_movie_id_error))

    override fun setImages(images: MutableList<String>) = movieImagesAdapter.setMovieImages(images)

    override fun showServerError() = toast(getString(R.string.server_error))

    override fun showNoInternetError() = toast(getString(R.string.no_internet_error))

    private fun goToMovieImageActivity(movieImageUrl: String, movieImage: ImageView) {
        val intent: Intent = MovieImageActivity.getLaunchIntent(this, movieImageUrl)
        var options: ActivityOptions? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            options = ActivityOptions.makeSceneTransitionAnimation(this, movieImage, getString(R.string.movie_image_transition))
        }
        startActivity(intent, options?.toBundle())
    }

    override fun goBack() = finish()

    override fun onPause() {
        presenter.unSubscribe()
        super.onPause()
    }
}