package com.example.zerinasever.moviesapp.common.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.example.zerinasever.moviesapp.R

fun Context.toast(message: String, duration: Int = Toast.LENGTH_SHORT) = Toast.makeText(this, message, duration).show()

fun Context.deleteFavouriteDialog(message: String, yesAction: () -> Unit, noAction: () -> Unit): AlertDialog =
        AlertDialog.Builder(this)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes)) { _, _ -> yesAction() }
                .setNegativeButton(getString(R.string.no)) { _, _ -> noAction() }
                .show()


fun Context.drawable(resource: Int): Drawable? = ContextCompat.getDrawable(this, resource)

fun Context.color(@ColorRes id: Int): Int = ContextCompat.getColor(this, id)