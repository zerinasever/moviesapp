package com.example.zerinasever.moviesapp.ui.movies.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.zerinasever.moviesapp.R
import com.example.zerinasever.moviesapp.data.model.Movie
import java.util.*

private const val NUMBER_OF_ITEMS = 20

class MoviesAdapter(private val onClick: (String) -> Unit,
                    private val onLastItemReached: () -> Unit = {}) : RecyclerView.Adapter<MoviesHolder>() {

    private val movies: MutableList<Movie> = ArrayList()

    fun setMovies(movies: List<Movie>) {
        this.movies.clear()
        this.movies.addAll(movies)
        notifyDataSetChanged()
    }

    fun addMoreMovies(movies: List<Movie>) {
        this.movies.addAll(movies)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = movies.size

    fun getMovieIdAtPosition(position: Int) = movies[position].id

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesHolder = MoviesHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_movie, parent, false))

    override fun onBindViewHolder(holder: MoviesHolder, position: Int) {
        val movie: Movie = movies[position]
        holder.setMovieData(movie)
        holder.setOnItemClickListener(onClick)
        handleItemPosition(position)
    }

    private fun handleItemPosition(position: Int) {
        if (position == itemCount - 1 && position > 0 && itemCount >= NUMBER_OF_ITEMS) {
            onLastItemReached.invoke()
        }
    }

    fun removeMovie(position: Int) {
        movies.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, movies.size)
    }
}