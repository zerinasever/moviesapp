package com.example.zerinasever.moviesapp.ui.splash

import android.os.Bundle
import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.R
import com.example.zerinasever.moviesapp.common.extensions.toast
import com.example.zerinasever.moviesapp.ui.base.BaseActivity
import com.example.zerinasever.moviesapp.ui.main.MainActivity
import javax.inject.Inject

class SplashActivity : BaseActivity(), SplashContract.View {

    @Inject
    lateinit var presenter: SplashContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.get().getComponent().inject(this)
        presenter.setView(this)
        initUi()
    }

    private fun initUi() = presenter.createGuestSession()

    override fun goToMain() {
        finish()
        startActivity(MainActivity.getLaunchIntent(this))
    }

    override fun showNoInternetError() = toast(message = getString(R.string.no_internet_error))

    override fun onPause() {
        presenter.unSubscribe()
        super.onPause()
    }
}