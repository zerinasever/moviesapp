package com.example.zerinasever.moviesapp.presentation

import com.example.zerinasever.moviesapp.common.utils.getFavouriteMoviesFromDatabase
import com.example.zerinasever.moviesapp.data.model.Movie
import com.example.zerinasever.moviesapp.data.realm.RealmMovieModel
import com.example.zerinasever.moviesapp.database.DatabaseManager
import com.example.zerinasever.moviesapp.ui.favouriteMovies.FavouriteMoviesContract

class FavouriteMoviesPresenter(private val database: DatabaseManager) : FavouriteMoviesContract.Presenter {

    private lateinit var view: FavouriteMoviesContract.View

    override fun setView(view: FavouriteMoviesContract.View) {
        this.view = view
    }

    override fun getFavouriteMovies() {
        val realmFavouriteMovies: List<RealmMovieModel>? = database.getFavouriteMovies()
        realmFavouriteMovies?.run {
            val favouriteMovies: List<Movie>? = getFavouriteMoviesFromDatabase(this)
            if (favouriteMovies != null && !favouriteMovies.isEmpty()) {
                view.showFavouriteMovies(favouriteMovies)
            } else {
                view.showNoFavouritesText()
            }
        }
    }

    override fun cancelDeletionFavourite(position: Int) = view.cancelSwipe(position)

    override fun clickedDeleteFavourite(position: Int, movieId: String) {
        val isRemoved = database.removeFavouriteMovie(movieId)
        if (isRemoved) {
            view.deleteFavourite(position)
        }
    }

    override fun itemClicked(movieId: String) = view.openMovieDetailsActivity(movieId)

    override fun iconBackClicked() = view.goBack()
}