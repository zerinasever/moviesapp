package com.example.zerinasever.moviesapp.ui.movieDetails

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.example.zerinasever.moviesapp.App
import com.example.zerinasever.moviesapp.R
import com.example.zerinasever.moviesapp.common.constants.IMAGE_PATH_ORIGINAL_SIZE
import com.example.zerinasever.moviesapp.common.constants.MOVIE_ID
import com.example.zerinasever.moviesapp.common.extensions.*
import com.example.zerinasever.moviesapp.common.utils.getYearFormat
import com.example.zerinasever.moviesapp.common.utils.loadImage
import com.example.zerinasever.moviesapp.common.utils.roundRating
import com.example.zerinasever.moviesapp.ui.base.BaseActivity
import com.example.zerinasever.moviesapp.ui.movieImages.MovieImagesActivity
import kotlinx.android.synthetic.main.activity_movie_details.*
import java.util.*
import javax.inject.Inject

class MovieDetailsActivity : BaseActivity(), MovieDetailsContract.View {

    @Inject
    lateinit var presenter: MovieDetailsContract.Presenter

    companion object {

        fun getLaunchIntent(from: Context, movieId: String): Intent {
            val intent = Intent(from, MovieDetailsActivity::class.java)
            intent.putExtra(MOVIE_ID, movieId)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.get().getComponent().inject(this)
        setContentView(R.layout.activity_movie_details)
        presenter.setView(this)
        getExtras()
        initUi()
    }

    private fun getExtras() {
        val bundle: Bundle? = intent.extras
        bundle?.run { presenter.setMovieId(bundle.getString(MOVIE_ID)) }
    }

    private fun initUi() {
        presenter.checkIsMovieFavourite()
        presenter.getMovieDetails()
        imagesButton.onClick { presenter.imagesButtonClicked() }
        iconBack.onClick { presenter.iconBackClicked() }
        movieHomepage.onClick { presenter.homepageClicked() }
        iconFavourite.onClick { presenter.iconFavouriteClicked() }
    }

    override fun showNoInternetError() = toast(getString(R.string.no_internet_error))

    override fun showRemoveFavouriteIcon() = iconFavourite.setImageDrawable(drawable(R.drawable.icon_favorite_empty))

    override fun showFavouriteIcon() = iconFavourite.setImageDrawable(drawable(R.drawable.icon_favourite_filled))

    override fun showTitle(title: String, date: String) {
        movieTitle.text = String.format(Locale.getDefault(), getString(R.string.movie_title_format), title, getYearFormat(date))
    }

    override fun showImage(posterPath: String) {
        loadImage(movieImage, IMAGE_PATH_ORIGINAL_SIZE + posterPath)
    }

    override fun showTagline(taglineText: String) {
        tagline.text = taglineText
    }

    override fun hideTagline() {
        taglineLabel.fadeOut()
        tagline.fadeOut()
    }

    override fun showHomepage(homepageText: String) {
        movieHomepage.text = homepageText.underlineText()
    }

    override fun showOverview(overviewText: String) {
        overview.text = overviewText
    }

    override fun showGenres(genresText: String) {
        genres.text = genresText
    }

    override fun showRating(voteAverage: Float) {
        rating.rating = voteAverage
        ratingText.text = String.format(Locale.getDefault(), getString(R.string.rating_format), roundRating(voteAverage, 1))
    }

    override fun hideHomepage() {
        homepageLabel.fadeOut()
        movieHomepage.fadeOut()
    }

    override fun showServerError() = toast(getString(R.string.server_error))

    override fun goToMovieImagesActivity(movieId: String?) = startActivity(MovieImagesActivity.getLaunchIntent(this, movieId))

    override fun goToWebsite(homepage: String) = startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(homepage)))

    override fun goBack() = finish()

    override fun onPause() {
        presenter.unSubscribe()
        super.onPause()
    }
}