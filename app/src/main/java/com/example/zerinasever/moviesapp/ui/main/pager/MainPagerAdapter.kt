package com.example.zerinasever.moviesapp.ui.main.pager

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.ArrayList

private const val UPCOMING = "Upcoming"
private const val POPULAR = "Popular"
private const val TOP_RATED = "Top rated"

class MainPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    private val fragments = ArrayList<Fragment>()
    private val titles = arrayOf(UPCOMING, POPULAR, TOP_RATED)

    fun addItem(fragment: Fragment) {
        fragments.add(fragment)
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

    override fun getPageTitle(position: Int): CharSequence = titles[position]
}