package com.example.zerinasever.moviesapp.data.response

import com.example.zerinasever.moviesapp.data.model.Image

data class MovieImagesResponse(val id: Int = 0,
                               val backdrops: MutableList<Image> = mutableListOf(),
                               val posters: MutableList<Image> = mutableListOf())