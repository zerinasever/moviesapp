package com.example.zerinasever.moviesapp.common.extensions

import android.view.View

private const val ZERO = 0
private const val MEDIUM_DURATION = 300

fun View.fadeIn(duration: Int = MEDIUM_DURATION, animDelay: Int = ZERO) {
    animate()
            .alpha(1f)
            .setStartDelay(animDelay.toLong())
            .setDuration(duration.toLong())
            .withStartAction { visibility = View.VISIBLE }
}

fun View.fadeOut(duration: Int = MEDIUM_DURATION, animDelay: Int = ZERO) {
    animate()
            .alpha(0f)
            .setStartDelay(animDelay.toLong())
            .setDuration(duration.toLong())
            .withStartAction { visibility = View.GONE }
}