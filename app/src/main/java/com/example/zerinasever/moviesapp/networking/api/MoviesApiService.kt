package com.example.zerinasever.moviesapp.networking.api

import com.example.zerinasever.moviesapp.data.response.*
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesApiService {

    @GET("authentication/token/new")
    fun getUserToken(@Query("api_key") apiKey: String): Single<TokenResponse>

    @GET("authenticate/{REQUEST_TOKEN}")
    fun authenticateUser(@Path("REQUEST_TOKEN") requestToken: String, @Query("api_key") apiKey: String): Single<TokenResponse>

    @GET("authentication/session/new")
    fun createSession(@Query("api_key") apiKey: String, @Query("request_token") requestToken: String): Single<SessionResponse>

    @GET("authentication/guest_session/new")
    fun createGuestSession(@Query("api_key") apiKey: String): Single<GuestSessionResponse>

    @GET("search/movie")
    fun searchMovies(@Query("api_key") apiKey: String, @Query("query") query: String): Single<MoviesResponse>

    @GET("movie/{movie_id}")
    fun getMovieDetails(@Path("movie_id") movieId: String, @Query("api_key") apiKey: String): Single<MovieResponse>

    @GET("movie/{movie_id}/images")
    fun getMovieImages(@Path("movie_id") movieId: String, @Query("api_key") apiKey: String): Single<MovieImagesResponse>

    @GET("movie/upcoming")
    fun getUpcomingMovies(@Query("api_key") apiKey: String, @Query("page") page: Int): Single<MoviesResponse>

    @GET("movie/popular")
    fun getPopularMovies(@Query("api_key") apiKey: String, @Query("page") page: Int): Single<MoviesResponse>

    @GET("movie/top_rated")
    fun getTopRatedMovies(@Query("api_key") apiKey: String, @Query("page") page: Int): Single<MoviesResponse>
}