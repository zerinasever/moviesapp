package com.example.zerinasever.moviesapp.data.response

import com.google.gson.annotations.SerializedName

data class SessionResponse(val success: Boolean = false,
                           @SerializedName("session_id")
                           val sessionId: String = "")