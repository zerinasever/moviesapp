package com.example.zerinasever.moviesapp.presentation

import com.example.zerinasever.moviesapp.common.utils.getGenres
import com.example.zerinasever.moviesapp.data.response.MovieResponse
import com.example.zerinasever.moviesapp.data.response.mapToRealmMovieModel
import com.example.zerinasever.moviesapp.database.DatabaseManager
import com.example.zerinasever.moviesapp.networking.interaction.MoviesInteractor
import com.example.zerinasever.moviesapp.ui.movieDetails.MovieDetailsContract
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.disposables.CompositeDisposable
import java.io.IOException

class MovieDetailsPresenter(private val moviesInteractor: MoviesInteractor,
                            private val database: DatabaseManager) : MovieDetailsContract.Presenter {

    private lateinit var view: MovieDetailsContract.View
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private var movieId: String = ""
    private var movieHomepage: String = ""
    private var movieResponse: MovieResponse? = null
    private var isMovieFavourite: Boolean = false

    override fun setView(view: MovieDetailsContract.View) {
        this.view = view
    }

    override fun setMovieId(id: String?) {
        id?.run { movieId = this }
    }

    override fun checkIsMovieFavourite() {
        isMovieFavourite = database.checkIfFavouriteExist(movieId)
        if (isMovieFavourite) {
            view.showFavouriteIcon()
        } else {
            view.showRemoveFavouriteIcon()
        }
    }

    override fun getMovieDetails() {
        val disposable = moviesInteractor.getMovieDetails(movieId,
                onSuccess = { handleMovieDetails(it) },
                onError = { handleError(it) })
        compositeDisposable.add(disposable)
    }

    private fun handleError(error: Throwable) {
        if (error is HttpException) {
            view.showServerError()
        } else if (error is IOException) {
            view.showNoInternetError()
        }
    }

    private fun handleMovieDetails(response: MovieResponse) = with(response) {
        movieResponse = this
        movieHomepage = homepage
        view.showTitle(title, releaseDate)
        view.showImage(backdropPath ?: "")
        view.showRating(voteAverage)
        view.showOverview(overview)
        view.showGenres(getGenres(genres))
        setTagline(tagline)
        setHomepage(homepage)
    }

    private fun setTagline(tagline: String) {
        if (tagline.isNotBlank()) {
            view.showTagline(tagline)
        } else {
            view.hideTagline()
        }
    }

    private fun setHomepage(homepage: String?) {
        homepage?.run {
            view.showHomepage(homepage)
        } ?: view.hideHomepage()
    }

    override fun imagesButtonClicked() = view.goToMovieImagesActivity(movieId)

    override fun iconBackClicked() = view.goBack()

    override fun homepageClicked() = view.goToWebsite(movieHomepage)

    override fun iconFavouriteClicked() {
        movieResponse?.run {
            val realmMovieModel = this.mapToRealmMovieModel()

            if (!isMovieFavourite) {
                isMovieFavourite = true
                database.addFavouriteMovie(realmMovieModel)
                view.showFavouriteIcon()
            } else {
                val isRemoved = database.removeFavouriteMovie(movieId)
                if (isRemoved) {
                    isMovieFavourite = false
                    view.showRemoveFavouriteIcon()
                }
            }
        }
    }

    override fun unSubscribe() = compositeDisposable.clear()
}