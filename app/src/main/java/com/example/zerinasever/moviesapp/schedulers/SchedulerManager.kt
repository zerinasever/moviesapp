package com.example.zerinasever.moviesapp.schedulers

import io.reactivex.Scheduler

interface SchedulerManager {

    fun mainThread(): Scheduler

    fun io(): Scheduler
}