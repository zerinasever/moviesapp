package com.example.zerinasever.moviesapp.data.realm

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class RealmMovieModel(@PrimaryKey
                           var id: String = "",
                           @SerializedName("poster_path")
                           var poster: String = "",
                           var title: String = "",
                           @SerializedName("vote_average")
                           var voteAverage: Float = 0f) : RealmObject()