package com.example.zerinasever.moviesapp.networking.interaction

import com.example.zerinasever.moviesapp.data.response.*
import io.reactivex.disposables.Disposable

interface MoviesInteractor {

    fun getUserToken(onSuccess: (TokenResponse) -> Unit, onError: (Throwable) -> Unit): Disposable

    fun authenticateUser(requestToken: String, onSuccess: (TokenResponse) -> Unit, onError: (Throwable) -> Unit): Disposable

    fun createGuestSession(onSuccess: (GuestSessionResponse) -> Unit, onError: (Throwable) -> Unit): Disposable

    fun createSession(requestToken: String, onSuccess: (SessionResponse) -> Unit, onError: (Throwable) -> Unit): Disposable

    fun searchMovies(query: String, onSuccess: (MoviesResponse) -> Unit, onError: (Throwable) -> Unit): Disposable

    fun getMovieDetails(movieId: String, onSuccess: (MovieResponse) -> Unit, onError: (Throwable) -> Unit): Disposable

    fun getUpcomingMovies(page: Int, onSuccess: (MoviesResponse) -> Unit, onError: (Throwable) -> Unit): Disposable

    fun getPopularMovies(page: Int, onSuccess: (MoviesResponse) -> Unit, onError: (Throwable) -> Unit): Disposable

    fun getTopRatedMovies(page: Int, onSuccess: (MoviesResponse) -> Unit, onError: (Throwable) -> Unit): Disposable

    fun getMovieImages(movieId: String, onSuccess: (MovieImagesResponse) -> Unit, onError: (Throwable) -> Unit): Disposable
}