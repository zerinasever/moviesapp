package com.example.zerinasever.moviesapp.ui.splash

import com.example.zerinasever.moviesapp.presentation.base.BasePresenter

interface SplashContract {

    interface View {

        fun goToMain()

        fun showNoInternetError()
    }

    interface Presenter : BasePresenter<View> {

        fun createGuestSession()

        fun unSubscribe()
    }
}