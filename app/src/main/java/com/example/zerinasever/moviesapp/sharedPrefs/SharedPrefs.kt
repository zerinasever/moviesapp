package com.example.zerinasever.moviesapp.sharedPrefs

interface SharedPrefs {

    fun setRequestToken(requestToken: String?)

    fun getRequestToken(): String?

    fun setGuestSessionId(sessionId: String?)

    fun getGuestSessionId(): String?
}